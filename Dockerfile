FROM golang:1.11.9-alpine3.9 as builder
WORKDIR /go
RUN apk add git
COPY ./src ./src
RUN go get -d -v ./src
RUN go build -o /go/bin/webapp ./src/

FROM alpine:3.9.3
WORKDIR /app
COPY --from=builder /go/bin/webapp /app/webapp
COPY ./template /app/template
COPY ./.env /app/.env
CMD ["/app/webapp"]