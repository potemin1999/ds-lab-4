package main

import (
	"database/sql"
	_ "github.com/lib/pq"
	"html/template"
	"net/http"
	"os"
)

type CounterModel struct {
	Counter int
}

var templ *template.Template
var templ2 *template.Template
var counterValue int
var counter *CounterModel
var db *sql.DB

func counterAddAndGet() int {
	counterValue++
	if db != nil {
		res, err := db.Query("SELECT nextval('public.\"visitor_counter\"');")
		if err != nil{
			println("DB error: ",err.Error())
		} else {
			res.Next()
			err = res.Scan(&counterValue)
			if err != nil {
				println("DB scan failed:",err.Error())
			}
		}
	}
	return counterValue
}

func requestHandler(w http.ResponseWriter, r *http.Request) {
	println(r.URL.Path)
	counter.Counter = counterAddAndGet()
	if r.URL.Path == "/" {
		println(r.URL.Path)
		err := templ.Execute(w, counter)
		if err != nil {
			println("Error while serving counter page:", err.Error())
		}
	} else if r.URL.Path == "/penguin.html" {
		err := templ2.Execute(w, counter)
		if err != nil {
			println("Error while serving penguin page:", err.Error())
		}
	} else {
		w.WriteHeader(404)
	}
}

func connectToDB() {
	var err error
	host := os.Getenv("POSTGRES_HOST")
	user := os.Getenv("POSTGRES_USERNAME")
	password := os.Getenv("POSTGRES_PASSWORD")
	dbName := os.Getenv("POSTGRES_DB")
	connStr := "postgres://" + user + ":" + password + "@"+host+"/" + dbName + "?sslmode=disable"
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		panic("Unable to connect to the database:" + err.Error())
	}else {
		println("Successfully connected to the database")
	}
}

func main() {
	var err error
	counter = &CounterModel{
		Counter: 0,
	}
	connectToDB()
	templ, err = template.ParseFiles("template/counter.html")
	templ2, err = template.ParseFiles("template/penguin.html")
	host := os.Getenv("WEBAPP_HOST")+":"+os.Getenv("WEBAPP_PORT")
	println("Starting server at "+host)
	err = http.ListenAndServe(host, http.HandlerFunc(requestHandler))
	if err != nil {
		println("Error while server startup: ", err.Error())
	}
}
